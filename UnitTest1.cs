using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using OpenQA.Selenium.Appium.Enums;
using OpenQA.Selenium.Appium.Interfaces;
using System;

namespace AutomationTestingBetaTV
{
    public class Tests
    {
        private AppiumDriver<AndroidElement> _driver;

        [SetUp]
        public void Setup()
        {
            var appPath = @"C:\Users\User\Documents\GitHub\if3250_2022_01_buletin\BuletinKlp01FE.Android\bin\Debug\com.companyname.buletinklp01fe.apk";

            var driverOption = new AppiumOptions();

            driverOption.AddAdditionalCapability(MobileCapabilityType.PlatformName, "Android");
            driverOption.AddAdditionalCapability(MobileCapabilityType.DeviceName, "Test");
            driverOption.AddAdditionalCapability(MobileCapabilityType.App, appPath);

            driverOption.AddAdditionalCapability("chromedriverexecutable", @"C:\Users\User\Downloads\chromedriver.exe");

            _driver = new AndroidDriver<AndroidElement>(new Uri("http://localhost:4723/wd/hub"), driverOption);

            var contexts = ((IContextAware)_driver).Contexts;
        }

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }
    }
}